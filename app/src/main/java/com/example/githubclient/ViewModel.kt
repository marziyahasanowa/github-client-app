package com.example.githubclient


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ViewModel: ViewModel() {
    private val apiService = RetrofitClient.apiService


    private val _repositories = MutableLiveData<List<Repository>>()
    val repositories: LiveData<List<Repository>> get() = _repositories

    private val _toastMessage = MutableLiveData<String>()
    val toastMessage: LiveData<String> get() = _toastMessage

    private fun showToast(message: String) {
        _toastMessage.postValue(message)
    }

    fun searchRepositories(organization: String) {
        viewModelScope.launch {
            try {
                val response = apiService.getRepositoriesForOrganization(organization)
                _repositories.value = response
            } catch (e: Exception) {
                showToast("No repos found for organization $organization")
            }
        }
    }

    private val _repositoriesDetails = MutableLiveData<RepositoryDetails>()
    val repositoriesDetails: LiveData<RepositoryDetails> get() = _repositoriesDetails


    fun getRepositoryDetails(owner: String, repo: String) {
        viewModelScope.launch {
            try {
                val response = apiService.getRepositoryDetails(owner, repo)
                _repositoriesDetails.value = response
            } catch (e: Exception) {
                showToast("Error fetching repository details")
            }
        }
    }



}