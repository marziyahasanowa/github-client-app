package com.example.githubclient

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.githubclient.databinding.RepositoryItemBinding

class RepositoryAdapter (
    private var repositories: List<Repository>,
    private val clickListener: OnItemClickListener
) : RecyclerView.Adapter<RepositoryViewHolder>() {

    private lateinit var binding: RepositoryItemBinding
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepositoryViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.repository_item, parent, false)
        binding = RepositoryItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return RepositoryViewHolder(view, clickListener)
    }

    override fun onBindViewHolder(holder: RepositoryViewHolder, position: Int) {
        holder.bindView(repositories[position], clickListener)
    }

    override fun getItemCount(): Int {
        return repositories.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun updateRepositories(newRepositories: List<Repository>) {
        repositories = newRepositories
        notifyDataSetChanged() // Notify the adapter that the data has changed
    }


    interface OnItemClickListener {
        fun onItemClick(repository: Repository)
    }

}