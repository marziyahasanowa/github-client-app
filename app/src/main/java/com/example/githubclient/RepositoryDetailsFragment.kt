package com.example.githubclient

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.viewModels
import com.example.githubclient.databinding.FragmentRepositoryDetailsBinding

class RepositoryDetailsFragment : Fragment() {

    private lateinit var binding: FragmentRepositoryDetailsBinding
    private val viewModel: ViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRepositoryDetailsBinding.inflate(layoutInflater)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.repositoriesDetails.observe(viewLifecycleOwner)  { details ->

            binding.repositoryContentName.text = details.name
            binding.repositoryDescription.text = details.description
            binding.forks.text = details.forks.toString()
            binding.watchers.text = details.watchers.toString()
            binding.openIssues.text = details.openIssues.toString()

            val parentRepositoryTextView = binding.parentRepository
            if (details.isFork) {
                parentRepositoryTextView.visibility = View.VISIBLE
                parentRepositoryTextView.text = binding.parentRepository.text
            } else {
                parentRepositoryTextView.visibility = View.GONE
            }
        }

        viewModel.getRepositoryDetails("ownerName", "repoName")

    }

}