package com.example.githubclient

data class RepositoryDetails(
    val name: String,
    val description: String?,
    val forks: Int,
    val watchers: Int,
    val openIssues: Int,
    val isFork: Boolean,
    val parentRepository: String?
)
