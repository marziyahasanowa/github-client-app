package com.example.githubclient

data class Repository (
    val name:String,
    val description: String?,
)