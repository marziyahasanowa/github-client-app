package com.example.githubclient

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.githubclient.databinding.RepositoryItemBinding

class RepositoryViewHolder (
    itemView: View,
    private val clickListener: RepositoryAdapter.OnItemClickListener
): RecyclerView.ViewHolder(itemView) {

    private lateinit var binding: RepositoryItemBinding //???

    private val repositoryNameTextView: TextView = itemView.findViewById(R.id.repository_name)
    private val descriptionTextView: TextView = itemView.findViewById(R.id.description)

    fun bindView(repository: Repository, clickListener: RepositoryAdapter.OnItemClickListener) {
        itemView.setOnClickListener { clickListener.onItemClick(repository) }
        repositoryNameTextView.text = repository.name
        descriptionTextView.text = repository.description ?: "No description available"
    }

}
