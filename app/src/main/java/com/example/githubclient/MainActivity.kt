package com.example.githubclient

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.githubclient.databinding.ActivityMainBinding
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity(), RepositoryAdapter.OnItemClickListener {

    private lateinit var binding: ActivityMainBinding
    private val recyclerView: RecyclerView by lazy{ binding.recyclerView}
    private val adapter = RepositoryAdapter(emptyList(), this)
    private val isLoading: ProgressBar by lazy { binding.progressButton }
    private val searchButton: Button by lazy {  binding.buttonSearch}


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)


        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter

        val viewModel = ViewModelProvider(this).get(ViewModel::class.java)


        viewModel.repositories.observe(this) { repositories ->
            adapter.updateRepositories(repositories)
        }

        viewModel.toastMessage.observe(this) { message ->
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        }

        val organizationInput = binding.organisationInput

        searchButton.setOnClickListener {
            lifecycleScope.launch{
                isLoading.isVisible = true
                val orgName = organizationInput.text.toString()
                viewModel.searchRepositories(orgName)
                isLoading.isVisible = false
            }
        }


    }

    override fun onItemClick(repository: Repository) {
        supportFragmentManager.beginTransaction().apply {
            Log.d("onItemClick", "function called")
            replace(
                binding.container.id, RepositoryDetailsFragment()
            )
//            addToBackStack(null) ?
            commit()
        }
    }
}